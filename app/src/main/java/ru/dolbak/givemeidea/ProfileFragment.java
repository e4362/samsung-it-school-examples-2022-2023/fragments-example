package ru.dolbak.givemeidea;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;


public class ProfileFragment extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    ImageView photo;
    TextView name, age, distance, description;
    Person person;
    public ProfileFragment(Person person) {
        this.person = person;
    }


    // TODO: Rename and change types and number of parameters
//    public static ProfileFragment newInstance(String param1, String param2) {
//        ProfileFragment fragment = new ProfileFragment();
//        Bundle args = new Bundle();
//        args.putString(ARG_PARAM1, param1);
//        args.putString(ARG_PARAM2, param2);
//        fragment.setArguments(args);
//        return fragment;
//    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_profile, container, false);
        photo = v.findViewById(R.id.imageView);
        name = v.findViewById(R.id.name);
        age = v.findViewById(R.id.age);
        distance = v.findViewById(R.id.distance);
        description = v.findViewById(R.id.description);


        age.setText(String.valueOf(person.age) + " лет");
        photo.setImageDrawable(getResources().getDrawable(person.image));
        name.setText(person.name);
        distance.setText(String.valueOf(person.distance) + " метров от вас");
        description.setText(person.description);
        return v;
    }
}