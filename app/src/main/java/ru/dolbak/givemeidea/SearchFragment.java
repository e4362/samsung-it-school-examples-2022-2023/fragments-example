package ru.dolbak.givemeidea;


import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link SearchFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class SearchFragment extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    int personId = 0;

    Person[] persons;
    ImageView btnLeft, btnRight, photo, btnCheck;
    FragmentManager fm;
    public SearchFragment(FragmentManager fm) {
        this.fm = fm;
    }


    // TODO: Rename and change types and number of parameters
//    public static SearchFragment newInstance(String param1, String param2) {
//        SearchFragment fragment = new SearchFragment();
//        Bundle args = new Bundle();
//        args.putString(ARG_PARAM1, param1);
//        args.putString(ARG_PARAM2, param2);
//        fragment.setArguments(args);
//        return fragment;
//    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
        persons = new Person[10];
        persons[0] = new Person("Alex", 25, "Незабываемый мужчина", R.drawable.m1, 500);
        persons[1] = new Person("Alex", 25, "Незабываемый мужчина", R.drawable.m2, 500);
        persons[2] = new Person("Alex", 25, "Незабываемый мужчина", R.drawable.m3, 500);
        persons[3] = new Person("Alex", 25, "Незабываемый мужчина", R.drawable.m4, 500);
        persons[4] = new Person("Alex", 25, "Незабываемый мужчина", R.drawable.m5, 500);
        persons[5] = new Person("Alex", 25, "Незабываемый мужчина", R.drawable.f1, 500);
        persons[6] = new Person("Alex", 25, "Незабываемый мужчина", R.drawable.f2, 500);
        persons[7] = new Person("Alex", 25, "Незабываемый мужчина", R.drawable.f3, 500);
        persons[8] = new Person("Alex", 25, "Незабываемый мужчина", R.drawable.f4, 500);
        persons[9] = new Person("Alex", 25, "Незабываемый мужчина", R.drawable.f5, 500);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_search, container, false);
        btnLeft = v.findViewById(R.id.left);
        btnRight = v.findViewById(R.id.right);
        btnCheck = v.findViewById(R.id.check);
        photo = v.findViewById(R.id.photo);

        btnLeft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (personId > 0){
                    personId--;
                }
                else{
                    personId = persons.length - 1;
                }
                drawPeople();
            }
        });

        btnRight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                personId += 1;
                personId %= persons.length;
                drawPeople();
            }
        });

        btnCheck.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentTransaction ft = fm.beginTransaction();
                ProfileFragment pf = new ProfileFragment(persons[personId]);
                ft.replace(R.id.frameLayout, pf);
                ft.commit();
            }
        });
        drawPeople();
        return v;
    }

    void drawPeople(){
        photo.setImageDrawable(getResources().getDrawable(persons[personId].image));
    }
}