package ru.dolbak.givemeidea;

public class Person {
    String name;
    int age;
    String description;
    int image;
    int distance;

    public Person(String name, int age, String description, int image, int distance) {
        this.name = name;
        this.age = age;
        this.description = description;
        this.image = image;
        this.distance = distance;
    }

    public Person() {
    }
}